'use strict';
const { body, validationrslt, check, validationResult } = require('express-validator');
var models = require('../models/');
var rol = models.rol;
var detfactura = models.det_factura;
const bcrypt = require('bcrypt');
const saltRounds = 8;
var factura = models.factura;
var persona = models.persona;
var auto = models.auto;

class Det_facturaController {
    async listar(req, res) {
        var lista = await factura.findAll({
            //include: { model: models.cuenta, as: "cuenta", attributes: ['correo'] },
            attributes: ['motodpago', 'fecha', 'total']
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async guardarfac(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let persona_id = req.body.external_persona;
            let auto_id = req.body.external_auto;
            //console.log("personasdd"+persona_id);
            //console.log("autoo"+auto_id);


            if (auto_id != undefined) {
                let personaAux = await persona.findOne({ where: { external_id: persona_id } });
                let autoAux = await auto.findOne({ where: { external_id: auto_id } });
                const ivaplus = autoAux.precio * 0.12;
                const preciototal = autoAux.precio + ivaplus;
                //console.log("esto me devuelde",autoAux);
                if (autoAux && personaAux) {
                    var data = {
                        motodpago: req.body.motodpago,
                        fecha: req.body.fecha,
                        total: preciototal,
                        estado: req.body.estado,
                        id_persona: personaAux.id,

                        det_factura:{
                            descripcion: "Modelo es : "+autoAux.modelo+" Motor: "+autoAux.motor,
                            preciounit: autoAux.precio,
                            subtotal: autoAux.precio,
                            
                        }
                    };
                    res.status(200);
                    let transaction = await models.sequelize.transaction();
                    try {
                        await factura.create(data, { include: [{ model: models.det_factura, as: "det_factura" }], transaction });
                        await transaction.commit();
                        res.json({ msg: "Se han registrado sus datos", code: 200 });
                        
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 })
                        } else {
                            res.json({ msg: error.message, code: 200 })
                        }

                    }
                }
            } else {
                res.status(400);
                res.json({ msg: "Datos no encontrados", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400 });

        }
    }

    async modificar(req, res) {
        var aut = await auto.findOne({ where: { external_id: req.body.external } })
        if (aut === null) {
            res.json({ msg: "no existe el registro", code: 400 });
        } else {
            var uuid = require('uuid');
            aut.identificacion = req.body.dni;
            aut.tipo_identificacion = req.body.tipo;
            aut.apellidos = req.body.apellidos;
            aut.nombres = req.body.nombres;
            aut.direccion = req.body.direccion;
            aut.external_id = uuid.v4();
            var rslt = await aut.save();
            if (rslt === null) {
                res.status(400);
                res.json({ msg: "No se han modificado sus datos", code: 200 });
            } else {
                res.status(200);
                res.json({ msg: "Se han modificado sus datos", code: 200 });
            }
        }
    }

    
}

module.exports = Det_facturaController;