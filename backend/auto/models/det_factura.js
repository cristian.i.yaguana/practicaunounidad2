'use strict';

module.exports = (sequelize, DataTypes) => {
    const det_factura = sequelize.define('det_factura', {
        descripcion: { type: DataTypes.STRING(1000), defaultValue: "NO_DATA" },
        preciounit: { type: DataTypes.DOUBLE, defaultValue: 0.0 },
        subtotal: { type: DataTypes.DOUBLE, defaultValue: 0.0 },
        
    }, { freezeTableName: true });


   det_factura.associate = function (models) {
        det_factura.belongsTo(models.auto, { foreignKey: 'id_auto' });
        det_factura.belongsTo(models.factura, { foreignKey: 'id_factura' });
    }

    //persona.associate = function(models){
    //    persona.belongsTo(models.rol, {foreignKey: 'id_rol'});
     //   persona.hasOne(models.cuenta,{foreignKey: 'id_persona', as: 'cuenta'});
    //}
    return det_factura;
};