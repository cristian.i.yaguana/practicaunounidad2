
'use strict';
const { UUIDV4, DOUBLE } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    const imagen = sequelize.define('imagen', {
        url: { type: DataTypes.STRING(1000), defaultValue: "NO_DATA" },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    }, {
        freezeTableName: true
    });
    imagen.associate = function (models){
        imagen.hasMany(models.auto, {foreignKey: 'id_imagen', as: 'auto'});
    }

    return imagen;
};