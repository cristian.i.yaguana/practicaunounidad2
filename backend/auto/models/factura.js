'use strict';

module.exports = (sequelize, DataTypes) => {
    const factura = sequelize.define('factura', {
        motodpago: { type: DataTypes.STRING(100), defaultValue: "" },
        fecha: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        total: { type: DataTypes.DOUBLE, defaultValue: 0.0 },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true }
    }, { freezeTableName: true });

    //persona.associate = function(models){
    //    persona.belongsTo(models.rol, {foreignKey: 'id_rol'});
     //   persona.hasOne(models.cuenta,{foreignKey: 'id_persona', as: 'cuenta'});
    //}

    factura.associate = function(models){
        factura.belongsTo(models.persona, {foreignKey: 'id_persona'});
        factura.hasMany(models.det_factura,{foreignKey: 'id_factura', as: 'det_factura'});
    }

    return factura;
};