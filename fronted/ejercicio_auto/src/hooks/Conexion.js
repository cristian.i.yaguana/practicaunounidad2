//const URL = "http://localhost/v1/index.php";
const URL = "http://localhost:3006/api";

export const InicioSesion = async (data) => {
    const headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
    };
    const datos = await (
        await fetch(URL + "/sesion", {
            method: "POST",
            headers: headers,
            body: JSON.stringify(data),
        })
    ).json();
    //console.log(datos);
    return datos;
};



export const Marcas = async (key) => {
    const cabeceras = {
        "x-api-token": key,
    };
    const datos = await (
        await fetch(URL + "/marca/num", {
            method: "GET",
            headers: cabeceras,
        })
    ).json();
    console.log(datos.status);
    return datos;
};
export const obtenerMarca = async (key) => {
    const cabeceras = {
        'x-api-token': key
    };
    const datos = await (await fetch(URL + "/marca", {
        method: "GET",
        headers: cabeceras,
    })).json();
    console.log(datos)
    return datos;
}
export const obtenerPersona = async (key) => {
    const cabeceras = {
        'x-api-token': key
    };
    const datos = await (await fetch(URL + "/listpersonas", {
        method: "GET",
        headers: cabeceras,
    })).json();
    console.log(datos)
    return datos;
}
export const obtenerAuto = async (key) => {
    const cabeceras = {
        'x-api-token': key
    };
    const datos = await (await fetch(URL + "/auto", {
        method: "GET",
        headers: cabeceras,
    })).json();
    console.log(datos)
    return datos;
}



export const Autos = async (key) => {
    const datos = await (
        await fetch(URL + "/auto", {
            method: "GET",
        })
    ).json();
    //console.log(datos.status);
   // console.log(datos);
    return datos;
    
};
export const Factura = async (key) => {
    const datos = await (
        await fetch(URL + "/lfactura", {
            method: "GET",
        })
    ).json();
    //console.log(datos.status);
   // console.log(datos);
    return datos;
    
};

export const AutosCant = async (key) => {
    const cabeceras = {
        "x-api-token": key,
    };
    const datos = await (
        await fetch(URL + "/auto/num", {
            method: "GET",
            headers: cabeceras,
        })
    ).json();
    return datos;
};

export const ObtenerColores = async (key) => {
    const datos = await (
        await fetch(URL + "/auto/colores", {
            method: "GET",
        })
    ).json();
    return datos;
};

export const GuardarMarcas = async (data, key) => {
    const headers = {
        Accept: "application/json",
        "x-api-token": key,
    };
    const datos = await (
        await fetch(URL + "/marca/guardar", {
            method: "POST",
            headers: headers,
            body: JSON.stringify(data),
        })
    ).json();
    //console.log(datos);
    return datos;
};

export const GuardarAuto = async (data, key) => {
    const headers = {
        "x-api-token": key,
        'Accept': 'application/json',
        'Content-Type': 'application/json'

    };
    const datos = await (
        await fetch(URL + "/auto/guardar", {
            method: "POST",
            headers: headers,
            body: JSON.stringify(data),
        })
    ).json();
    //console.log(datos);
    return datos;
};
export const GuardarAutoimg=async(images,car,key)=>{
    const formData=new FormData();
    for(let i=0;i<images.length;i++){
        formData.append('images',images[i]);
    }
    formData.append('car', JSON.stringify(car));
}

export const GuardarFactura = async (data, key) => {
    const headers = {
        "x-api-token": key,
        'Accept': 'application/json',
        'Content-Type': 'application/json'

    };
    const datos = await (
        await fetch(URL + "/detfactura/venta", {
            method: "POST",
            headers: headers,
            body: JSON.stringify(data),
        })
    ).json();
    //console.log(datos);
    return datos;
};

