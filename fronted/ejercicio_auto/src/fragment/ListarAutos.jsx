import React, { useState } from 'react';
import { Autos } from '../hooks/Conexion';
import { useNavigate } from 'react-router-dom';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import Header from './Header';
import Footer from "./Footer";
import DataTable from 'react-data-table-component';
import { get } from 'react-hook-form';

const ListarAutos = () => {
    const navegation = useNavigate();
    const [data, setData] = useState([]);
    const [autolist, setAutoList] = useState(false);

    if (!autolist) {
        Autos(getToken()).then((info) => {
            if (info.code !== 200 && info.msg === 'Acceso denegado. Token ha expirado') {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion");
            } else {
                // console.log(info.data.length);
                setData(info.info);
                setAutoList(true);
                console.log(info.info);
            }


        });
    }

    Autos().then((info) => {
        if (info.error === false) {
            setData(info.data);
        };
    });

    const columns = [
        {
            name: 'Modelo',
            selector: row => row.modelo,
        },
        {
            name: 'Anio',
            selector: row => row.anio,
        },
        {
            name: 'Color',
            selector: row => row.color,
        },
        {
            name: 'Placa',
            selector: row => row.placa,
        },
        {
            name: 'Motor',
            selector: row => row.motor,
        },
        {
            name: 'Estado Vendido',
            selector: row => row.estado.toString(),
        },

        {
            name: 'Precio',
            selector: row => '$ ' + row.precio,
        },
    ];

    return (

        <div className='wrapper'>
            <Header />

            <div className="card text-center" style={{ margin: 20 }}>
                <div className="card-header" style={{ color: "black" }}>*** LISTA DE AUTOS ***</div>
                <div className="card-body">
                    <div className='container-fluid'>
                        <a className="btn btn-success" href={'/auto/registro'}>NUEVO</a>
                        <DataTable columns={columns} data={data} />
                    </div>
                </div>
            </div>
            <Footer />
        </div>


    );
}

export default ListarAutos;