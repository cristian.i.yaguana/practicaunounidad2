import React, { useState } from 'react';
import { Factura } from '../hooks/Conexion';
import { useNavigate } from 'react-router-dom';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import Header from './Header';
import Footer from "./Footer";
import DataTable from 'react-data-table-component';
import { get } from 'react-hook-form';

const ListarFactura = () => {
    const navegation = useNavigate();
    const [data, setData] = useState([]);
    const [facturalist, setFacturaList] = useState(false);

    if (!facturalist) {
        Factura(getToken()).then((info) => {
            if (info.code !== 200 && info.msg === 'Acceso denegado. Token ha expirado') {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion");
            } else {
                // console.log(info.data.length);
                setData(info.info);
                setFacturaList(true);
                console.log(info.info);
            }


        });
    }

    Factura().then((info) => {
        if (info.error === false) {
            setData(info.data);
        };
    });

    const columns = [
        {
            name: 'motodpago',
            selector: row => row.motodpago,
        },
        {
            name: 'fecha',
            selector: row => row.fecha,
        },
        {
            name: 'total',
            selector: row => '$ ' +row.total,
        },

    ];

    return (

        <div className='wrapper'>
            <Header />

            <div className="card text-center" style={{ margin: 20 }}>
                <div className="card-header" style={{ color: "black" }}>*** LISTA DE FACTURAS ***</div>
                <div className="card-body">
                    <div className='container-fluid'>
                        <a className="btn btn-success" href={'/factura/registro'}>NUEVO</a>
                        <DataTable columns={columns} data={data} />
                    </div>
                </div>
            </div>
            <Footer />
        </div>


    );
}

export default ListarFactura;