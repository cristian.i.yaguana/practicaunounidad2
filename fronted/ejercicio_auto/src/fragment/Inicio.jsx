import Header from "./Header";
import "../css/style.css";
import Footer from "./Footer";
import { AutosCant, Marcas } from "../hooks/Conexion";
import { borrarSesion, getToken } from "../utilidades/Sessionutil";
import { useNavigate } from 'react-router-dom';
import mensajes from '../utilidades/Mensajes';
import { useState } from "react";
//import ListarAutos from "./ListarAutos";
//borrarSesion();

const Inicio = () => {
    const navegation = useNavigate();
    const [nro, setNro] = useState(0);
    const [nroA, setNroA] = useState(0);

    Marcas(getToken()).then((info) => {
        //console.log(info.error);
        if (info.code !== 200 && info.msg === 'Acceso denegado. Token ha expirado') {
            borrarSesion();
            mensajes(info.msg);
            navegation("/sesion");
        } else {
            // console.log(info.data.length);
            setNro(info.nummarcas);
            
        }
    });

    AutosCant(getToken()).then((info) => {
        if (info.code !== 200 && info.msg === 'Acceso denegado. Token ha expirado') {
            borrarSesion();
            mensajes(info.msg);
            navegation("/sesion");
        } else {
            //console.log(info.data.length);
            //console.log(info);
            setNroA(info.numautos);
            //console.log("holasscs",info.nro);
        }
    });

    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />

                    {/**DE AQUI CUERPO */}
                    <div className="container-fluid">
                        <h1 className="h3 mb-0 text-gray-800">Dashboard</h1>

                        <div className="row">
                            <dbiv className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-left-primary shadow h-100 py-2">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Nro de marcas</div>
                                                <div className="h5 mb-0 font-weight-bold text-gray-800">{nro}</div>
                                            </div>
                                            <div className="col-auto">
                                                <i className="fas fa-calendar fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </dbiv>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-left-success shadow h-100 py-2">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    Autos en existencia</div>
                                                <div className="h5 mb-0 font-weight-bold text-gray-800">{nroA}</div>
                                            </div>
                                            <div className="col-auto">
                                                <i className="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                    {/*<ListarAutos/>*/}
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default Inicio;