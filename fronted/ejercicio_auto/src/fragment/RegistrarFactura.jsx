import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useNavigate, Link } from 'react-router-dom';
import { obtenerPersona, obtenerAuto, GuardarAuto,GuardarFactura } from '../hooks/Conexion';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import Header from "./Header";
import { useForm } from 'react-hook-form';
import { Button } from 'bootstrap';

const RegistrarAuto = () => {
    const navegation = useNavigate();
    const [autos, setAutos] = useState([]);
    const [llautos, setLlautos] = useState(false);
    const [personas, setPersonas] = useState([]);
    const [llpersonas, setLlpersons] = useState(false);
    const { register, handleSubmit, formState: { errors } } = useForm();

    //acciones
    //submit
    const onSubmit = (data) => {

        var datos = {
            "fecha":data.fecha,
            "external_persona":data.external_persona,
            "external_auto":data.external_auto,
            "motodpago": data.motodpago,

        };
        GuardarFactura(datos, getToken()).then((info) => {
            if (info.code !== 200) {
                if (info.msg === 'token expirado o no valido') {
                    borrarSesion();
                    mensajes(info.msg);
                    navegation("/sesion");
                } else {
                    mensajes(info.msg, 'error', 'Error');
                }
            } else {
                if (info.msg === 'NO GENERADA') {
                    mensajes("Placa ya existente", 'error', 'Error');
                } else {
                    mensajes(info.msg, 'success', 'Success');
                    navegation('/auto');
                }
            }
        });
    };


    //lamar colores
    //if (!llcolor) {
    //    ObtenerColores().then((info) => {
    ///       if (info.error === false) {
    //        setColores(info.data);
    //  }
    // setLlcolor(true);
    // });
    //};

    //llamar marcas
    if (!llpersonas) {
        obtenerPersona(getToken()).then((info) => {
            //console.log(info.error);
            if (info.code !== 200 && info.msg === 'Acceso denegado. Token ha expirado') {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion");
            } else {
                setPersonas(info.info);
                setLlpersons(true);
            }
        });
    };
    if (!llautos) {
        obtenerAuto(getToken()).then((info) => {
            //console.log(info.error);
            if (info.code !== 200 && info.msg === 'Acceso denegado. Token ha expirado') {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion");
            } else {
                setAutos(info.info);
                setLlautos(true);
            }
        });
    };

    return (
        <div className="wrapper" >
            <center>
                <Header />
                <div className="d-flex flex-column" style={{ width: 700 }}>
                    <h5 className="title" style={{ color: "black", font: "bold" }}>REGISTRO DE FACTURA</h5>
                    <br />
                    <div className="container">motodpago
                        <img src="http://www.midasred.net/web/images/servicios/main-iconos/facturas.svg" className="card" style={{ width: 200, height: 125 }} />
                    </div>
                    <br />

                    <div className='container-fluid'>
                        <form className="user" onSubmit={handleSubmit(onSubmit)}>
                            <div className="row mb-4">
                                <div className="col">
                                    <input type="text" {...register('motodpago', { required: true })} className="form-control form-control-user" placeholder="Ingrese metodo de pago" />
                                    {errors.motodpago && errors.motodpago.type === 'required' && <div className='alert alert-danger'>Ingrese metodo de pago</div>}
                                </div>
                                <div className="col">
                                    <input type="date" className="form-control form-control-user" placeholder="Ingrese la fecha" {...register('fecha', { required: true })} />
                                    {errors.fecha && errors.fecha.type === 'required' && <div className='alert alert-danger'>Ingrese fecha</div>}
                                </div>
                            </div>


                            <div className="row mb-4">
                                <div className="col">
                                    <select className='form-control' {...register('external_auto', { required: true })}>
                                        <option>Elija el auto</option>
                                        {autos.map((m, i) => {
                                            return (<option key={i} value={m.external_id}>
                                                {m.modelo + " Placa: " + m.placa}
                                            </option>)
                                        })}
                            </select>
                            {errors.external_auto && errors.external_auto.type === 'required' && <div className='alert alert-danger'>Selecione un auto</div>}
                    </div>
                </div>
                <div className="row mb-4">
                    <div className="col">
                        <select className='form-control' {...register('external_persona', { required: true })}>
                            <option>Elija la Persona</option>
                            {personas.map((m, i) => {
                                return (<option key={i} value={m.external_id}>
                                    {m.nombres + " " + m.apellidos}
                                </option>)
                            })}
                        </select>
                        {errors.external_persona && errors.external_persona.type === 'required' && <div className='alert alert-danger'>Selecione una persona</div>}
                    </div>
                </div>
                <hr />
                <button type='submit' className="btn btn-success">GUARDAR</button>
                <Link to='/lfactura' className="btn btn-danger" style={{ margin: 20 }}>CANCELAR</Link>
            </form>
        </div>
                </div >
            </center >
        </div >
    );
}

export default RegistrarAuto;